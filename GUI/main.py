import os
import pygame
from my_module import card, scene, music

def run_game(width, height, fps, starting_scene):
    pygame.init()
    screen = pygame.display.set_mode((width, height))
    pygame.display.set_caption("Not only BlackJack")
    # Icona finestra
    # icon = pygame.image.load('C:\\Users\\aliga\\Desktop\\nob\\not_only_blackjack\\Code\\GUI\\img\\icon.png')
    # pygame.display.set_icon(icon)
    clock = pygame.time.Clock()
    active_scene = starting_scene

    base_path = os.path.dirname(__file__)
    path = f"{base_path}/sound/songs/"
    m = music.Music(path)
    # m.playMusic()

    while active_scene is not None:
        pressed_keys = pygame.key.get_pressed()
        # print(pressed_keys[pygame.K_RALT])

        # Event filtering
        filtered_events = []

        for event in pygame.event.get():
            quit_attempt = False

            # Controllo se viene chiusa la finestra
            if event.type is pygame.QUIT:
                quit_attempt = True
            elif event.type is pygame.KEYDOWN:
                # Controllo se vengono premute delle sequenze per chiudere la finestra
                alt_held = pressed_keys[pygame.K_LALT] or pressed_keys[pygame.K_RALT]
                ctrl_held = pressed_keys[pygame.K_LCTRL] or pressed_keys[pygame.K_RCTRL]

                if event.key is pygame.K_m and ctrl_held:
                    m.stopMusic()

                # Controllo se viene premuto ESC
                if event.key is pygame.K_ESCAPE:
                    quit_attempt = True
                # Problemi con ALT
                elif event.key is pygame.K_F4 and alt_held:
                    quit_attempt = True
                elif event.key is pygame.K_w and ctrl_held:
                    quit_attempt = True

            if quit_attempt:
                active_scene.terminate()
            else:
                filtered_events.append(event)

        active_scene.processInput(filtered_events, pressed_keys)
        active_scene.update()
        active_scene.render(screen)

        active_scene = active_scene.next

        pygame.display.flip()
        clock.tick(fps)

run_game(626, 417, 60, scene.StartScene())
