import os
import time
import pygame
from my_module import cache, card, characters

Cache = cache.Cache()
Dealer = object
Player = object
Deck = object

class SceneBase:
    """Generic class for creating all the scenes"""
    def __init__(self):
        self.next = self
        self.__base_path = os.path.dirname(os.path.dirname(__file__))

        self.__default_font = f"{self.__base_path}/font/harlowsi.ttf"

        self.font_preferences = [
                "Arial",
                "Papyrus",
                "Comic Sans MS"]
        # Image path
        self.__images = f"{self.__base_path}/img"
        self.__backgrounds = f"{self.__images}/backgrounds"
        self.__cards = f"{self.__images}/cards"
        self.__characters = f"{self.__images}/characters"
        self.__info = f"{self.__images}/info"
        self.__menu = f"{self.__images}/menu"
        self.__text = f"{self.__images}/text"

        # Sound path
        self.__effects = f"{self.__base_path}/sound/effects"

    @property
    def base_path(self):
        return self.__base_path

    @property
    def default_font(self):
        return self.__default_font

    @property
    def images(self):
        return self.__images

    @property
    def backgrounds(self):
        return self.__backgrounds

    @property
    def cards(self):
        return self.__cards

    @property
    def characters(self):
        return self.__characters

    @property
    def info(self):
        return self.__info

    @property
    def menu(self):
        return self.__menu

    @property
    def text(self):
        return self.__text

    @property
    def effects(self):
        return self.__effects

    def processInput(self, events, pressed_keys):
        pass

    def update(self):
        pass

    def render(self, screen):
        pass

    def switchToScene(self, next_scene):
        self.next = next_scene

    def terminate(self):
        self.switchToScene(None)

    def openMenu(self, screen):
        pass

    def closeMenu(self, screen):
        pass

    def formattingValue(self, val):
        # format() restituisce una stringa
        val_str = format(float(val), ".2f")
        val_str = val_str.replace('.', ',')
        val_str = list(val_str)

        if val >= 10000:
            val_str.insert(2, '.')
        elif val >= 1000:
            val_str.insert(1, '.')
        
        val_str = "".join(val_str)

        return val_str

    def textPosition(self, val, start, space):
        if val >= 10000:
            return start + space
        elif val >= 1000:
            space *= 2
            return start + space
        elif val >= 100:
            space *= 3
            return start + space
        elif val >= 10:
            space *= 4
            return start + space
        else:
            space *= 5
            return start + space

    def animationOpenMenu(self, start, end, cord_arrow, shift, left_to_right=False, right_to_left=False, up_to_bottom=False, bottom_to_up=False):
        # Arrow position
        arrow_position = (cord_arrow - end) + start

        if left_to_right or up_to_bottom:
            if start < end:
                start += shift
                if start > end:
                    start = end
        elif right_to_left or bottom_to_up:
            if start > end:
                start -= shift
                if start < end:
                    start = end

        return start, arrow_position

    def animationCloseMenu(self, start, end, cord_arrow, shift, left_to_right=False, right_to_left=False, up_to_bottom=False, bottom_to_up=False):
        if right_to_left or bottom_to_up:
            cord_arrow -= shift

            if start > end:
                start -= shift
                if start < end:
                    start = end
        elif left_to_right or up_to_bottom:
            cord_arrow += shift

            if start < end:
                start += shift
                if start > end:
                    start = end
        
        return start, cord_arrow

class StartScene(SceneBase):
    """ Starting scene
        
        Getter from SceneBase:
            base_path
            default_font
            images
            backgrounds
            cards
            characters
            info
            menu
            text
            effects
    """

    def __init__(self):
        super().__init__()
        # self._button = pygame.Rect(100, 200, 160, 50)
        self.__background = f"{super().backgrounds}/start.png"
        self.__freccia_icon = f"{super().menu}/freccia.png"

    def processInput(self, events, pressed_keys):
        # Una volta selezionata la voce -> in base all'index partirà una scena
        for event in events:
            if event.type is pygame.KEYDOWN:
                if event.key is pygame.K_RETURN:
                    self.switchToScene(ShuffleScene())
            # if event.type is pygame.MOUSEMOTION:
            #     mouse_pos = event.pos

            #     if self._button.collidepoint(mouse_pos):
                    # print(f'mouse hover {mouse_pos}')

    def render(self, screen):
        screen.blit(Cache.getImage(self.__background), (0, 0))
        screen.blit(Cache.getImage(self.__freccia_icon), (63.869, 226.412))

class ShuffleScene(SceneBase):
    """ Shuffling scene
        
        Getter from SceneBase:
            base_path
            default_font
            images
            backgrounds
            cards
            characters
            info
            menu
            text
            effects
    """
    def __init__(self):
        super().__init__()
        
        # Background
        self.__background = f"{super().backgrounds}/shuffle.png"
        
        # Text
        self.__dot_text = f"{super().text}/dot.png"
        self.__cord_dot = ( (445.377, 220.956), (465.573, 226.089), (485.558, 232.001) )
        self.__count_dot = 1

        # Cards
        self.__cards = f"{super().cards}/"

        # Sound
        self.__sound = f"{super().effects}/card_shuffling.wav"
        Cache.playEffect(self.__sound)

        # Set the Deck
        global Deck, Player, Dealer
        Deck = card.BlackJackDeck(6, self.__cards)
        Deck.shuffle()
        
        Player = characters.Player()
        Dealer = characters.Dealer()

    def render(self, screen):
        screen.blit(Cache.getImage(self.__background), (0, 0))

        # Text
        for i in range(self.__count_dot):
            if i < len(self.__cord_dot):
                screen.blit(Cache.getImage(self.__dot_text), (self.__cord_dot[i][0], self.__cord_dot[i][1]))

        if self.__count_dot >= len(self.__cord_dot):
            if not pygame.mixer.get_busy():
                self.switchToScene(MainMenu())

        self.__count_dot += 1
        time.sleep(0.7)

class MainMenu(SceneBase):
    """ Main menu scene
        
        Getter from SceneBase:
            base_path
            default_font
            images
            backgrounds
            cards
            characters
            info
            menu
            text
            effects
    """

    def __init__(self):
        super().__init__()
        self.__animation = False
        self.__close = False

        # Active menu
        self.__active_menu = {
            "main": True,
            "bet": False,
            "actions": False,
            "info": False,
            "save": False,
            "options": False,
            "exit": False,
        }

        # Background
        self.__background = f"{super().backgrounds}/main_menu.png"

        # Main menu
        self.__main_menu = f"{super().menu}/main_menu.png"
        self.__freccia_icon = f"{super().menu}/freccia.png"

        # Basic info
        self.__heart = f"{super().info}/heart.png"
        self.__hearts = ((592.665, 105.066), (577.284, 111.902), (560.195, 115.320), (543.106, 111.902), (527.726, 105.066))

        # Character
        self.__character = f"{super().characters}/01.png"

        # Freccia position
        self.__shift = 0
        self.__index_x_freccia = 0
        self.__index_y_freccia = 0

        self.__x_sub_menu = 0

        global Player, Dealer
        Player.clearHand()
        Dealer.clearHand()

    @property
    def background(self):
        return self.__background

    @property
    def main_menu(self):
        return self.__main_menu

    @property
    def freccia_icon(self):
        return self.__freccia_icon

    @property
    def heart(self):
        return self.__heart

    @property
    def hearts(self):
        return self.__hearts

    @property
    def character(self):
        return self.__character

    def processInput(self, events, pressed_keys):
        # [38, 102, 161, 233, 298, 363] -> in base all'index partirà una scena
        for event in events:
            if event.type is pygame.KEYDOWN: #and event.key is pygame.K_s:
                self.choiceMenu(event, pressed_keys)

            # if event.type is pygame.MOUSEMOTION:
            #     mouse_pos = event.pos

            #     if self.bet_button.collidepoint(mouse_pos):
            #         self.switchToScene(OpenBetMenu())
            #     elif self.action_button.collidepoint(mouse_pos):
            #         self.switchToScene(SelectActionMenu())
            #     elif self.info_button.collidepoint(mouse_pos):
            #         self.switchToScene(SelectInfoMenu())
            #     elif self.save_button.collidepoint(mouse_pos):
            #         self.switchToScene(SelectSaveMenu())
            #     elif self.options_button.collidepoint(mouse_pos):
            #         self.switchToScene(SelectOptionsMenu())
            #     elif self.exit_button.collidepoint(mouse_pos):
            #         self.switchToScene(SelectExitMenu())

    def render(self, screen):
        global Player

        SPACE = 9.57
        START = 523.010

        # Background
        screen.blit(Cache.getImage(self.__background), (0, 0))

        # Main menu
        if(self.__active_menu.get("main") and not self.__close):
            screen.blit(Cache.getImage(self.__main_menu), (0, 12.071))
            self.openMenu(screen)
        elif(self.__close):
            self.closeMenu(screen)
            screen.blit(Cache.getImage(self.__main_menu), (0, 12.071))
        else:
            self.openMenu(screen)
            screen.blit(Cache.getImage(self.__main_menu), (0, 12.071))

        # Info
        money = Cache.createTextDefaultFont(self.formattingValue(Player.money), self.default_font, 21, (0, 0, 0))
        bet = Cache.createTextDefaultFont(self.formattingValue(Player.bet), self.default_font, 21, (0, 0, 0))

        # Life
        for i in range(Player.life):
            screen.blit(Cache.getImage(self.__heart), self.__hearts[i])
        
        screen.blit(money, (self.textPosition(Player.money, START, SPACE), 232))
        screen.blit(bet, (self.textPosition(Player.bet, START, SPACE), 361))
        
        # Avatar
        screen.blit(Cache.getImage(self.__character), (535.884, 25.293))

    def openMenu(self, screen):
        menus = list(self.__active_menu.keys())

        for m in menus:
            if self.__active_menu.get(m):
                if m == "main":
                    y_freccia = (40.589, 106.414, 171.425, 239.408, 304.419, 370.413)
                    screen.blit(Cache.getImage(self.__freccia_icon), (9, y_freccia[self.__index_y_freccia]))

                    return
                elif m == "bet":
                    menu = f"{super().menu}/bet_menu.png"
                    x_freccia = (93.495, 210.157)
                    y_freccia = (70.614, 159.026, 250.614, 339.026)

                    if self.__animation:
                        # Positions for sub-menu animation
                        MENU_POS = 67.408
                        self.__x_sub_menu, arrow_position = self.animationOpenMenu(self.__x_sub_menu, MENU_POS, x_freccia[self.__index_x_freccia], 25, left_to_right=True)

                        # Sub Menu
                        screen.blit(Cache.getImage(menu), (self.__x_sub_menu, 22.000))
                        screen.blit(Cache.getImage(self.freccia_icon), (arrow_position, y_freccia[self.__index_y_freccia]))

                        if self.__x_sub_menu == MENU_POS:
                            self.__animation = False
                    else:
                        # Sub Menu
                        self.__shift = x_freccia[self.__index_x_freccia]
                        screen.blit(Cache.getImage(menu), (self.__x_sub_menu, 22.000))
                        screen.blit(Cache.getImage(self.freccia_icon), (x_freccia[self.__index_x_freccia], y_freccia[self.__index_y_freccia]))
                    
                    return

    def closeMenu(self, screen):
        global Player

        menus = list(self.__active_menu.keys())

        for m in menus:
            if self.__active_menu.get(m):
                if m == "bet":
                    menu = f"{super().menu}/bet_menu.png"
                    y_freccia = (70.614, 159.026, 250.614, 339.026)

                    MENU_POS = -248

                    if self.__x_sub_menu > MENU_POS:
                        # Positions for sub-menu animation
                        self.__x_sub_menu, self.__shift = self.animationCloseMenu(self.__x_sub_menu, MENU_POS, self.__shift, 25, right_to_left=True)

                        # Sub Menu
                        screen.blit(Cache.getImage(menu), (self.__x_sub_menu, 22.000))
                        screen.blit(Cache.getImage(self.__freccia_icon), (self.__shift, y_freccia[self.__index_y_freccia]))
                    else:
                        if Player.bet:
                            self.switchToScene(DealingScene(True))
                        else:
                            self.__active_menu[m] = False
                            self.__active_menu["main"] = True
                            self.__close = False
                            self.__index_y_freccia = 0
                    
                    return

    def choiceMenu(self, event, pressed_keys):
        menus = list(self.__active_menu.keys())

        for m in menus:
            if self.__active_menu.get(m):
                if m == "main":
                    y_freccia = 5
                    if event.key is pygame.K_s:
                        if self.__index_y_freccia < y_freccia:
                            self.__index_y_freccia += 1
                        else:
                            self.__index_y_freccia = 0
                    elif event.key is pygame.K_w:
                        if self.__index_y_freccia > 0:
                            self.__index_y_freccia -= 1
                        else:
                            self.__index_y_freccia = y_freccia
                    elif event.key is pygame.K_RETURN:
                        self.__animation = True
                        self.__index_y_freccia = 0
                        self.__x_sub_menu = -246
                        self.__active_menu[m] = False
                        self.__active_menu[menus[self.__index_y_freccia + 1]] = True

                    return
                elif m == "bet":
                    y_freccia = 3
                    x_freccia = 1
                    values = [(5, 10), (50, 100), (500, 1000), [Player.money, Player.clearBet]]

                    if event.key is pygame.K_s:
                        if self.__index_y_freccia < y_freccia:
                            self.__index_y_freccia += 1
                        else:
                            self.__index_y_freccia = 0
                    elif event.key is pygame.K_w:
                        if self.__index_y_freccia > 0:
                            self.__index_y_freccia -= 1
                        else:
                            self.__index_y_freccia = y_freccia
                    elif event.key is pygame.K_a:
                        if self.__index_x_freccia > 0:
                            self.__index_x_freccia -= 1
                        else:
                            self.__index_x_freccia = x_freccia
                    elif event.key is pygame.K_d:
                        if self.__index_x_freccia < x_freccia:
                            self.__index_x_freccia += 1
                        else:
                            self.__index_x_freccia = 0
                    elif event.key is pygame.K_RETURN:
                        if (self.__index_y_freccia == 3) and (self.__index_x_freccia == 1):
                            values[self.__index_y_freccia][self.__index_x_freccia]()
                        else:
                            Player.betting(values[self.__index_y_freccia][self.__index_x_freccia])

                        values[3][0] = Player.money
                    elif event.key is pygame.K_BACKSPACE:
                        self.__animation = True
                        self.__x_sub_menu = 67.408
                        self.__close = True

                    return

class DealingScene(MainMenu):
    """ Dealing scene
        
        Getter from SceneBase:      Getter from MainMenu:
            base_path                   background
            default_font                main_menu
            images                      freccia_icon
            backgrounds                 heart
            cards                       hearts
            characters                  character
            info
            menu
            text
            effects
    """

    def __init__(self, first_call=False, dealer_hand=False):
        super().__init__()
        self.__first_call = first_call
        self.__dealer_hand = dealer_hand
        self.__animation = True
        self.__close = False

        # Active menu
        self.__active_menu = {
            "blackjack": True,
            "bet": False,
            "actions": False,
            "info": False,
            "save": False,
            "options": False,
            "exit": False,
        }

        # Bet menu
        self.__bj_menu = f"{super().menu}/bj_menu.png"

        # self.__x_freccia = (165.695, 267.464, 362.291)
        # self.__y_freccia = 383.667
        self.__shift = 0
        self.__index_x_freccia = 0
        self.__index_y_freccia = 0

        self.__y_sub_menu = 417

        # Sound
        self.__sound = f"{super().effects}/flip_card-[AudioTrimmer.com].wav"

    def processInput(self, events, pressed_keys):
        global Player

        for event in events:
            if event.type is pygame.KEYDOWN:
                self.choiceMenu(event, pressed_keys)

    def update(self):
        if Player.hasBlackJack():
            if Dealer.hand["value"] != 11 and Dealer.hand["value"] != 10:
                Player.winner()
                self.switchToScene(MainMenu())
            # if Dealer.hand["value"] == 11:

    def render(self, screen):
        global Player, Dealer, Deck
        
        SPACE = 9.57
        START = 523.010

        # Background
        screen.blit(Cache.getImage(super().background), (0, 0))
        
        # Deal the first three cards
        self.baseHand()

        # Dealing
        self.dealing(screen)

        # Main menu
        if(self.__active_menu.get("blackjack") and not self.__close):
            screen.blit(Cache.getImage(super().main_menu), (0, 12.071))
            self.openMenu(screen)
        elif(self.__close):
            self.closeMenu(screen)
            screen.blit(Cache.getImage(super().main_menu), (0, 12.071))
        else:
            self.openMenu(screen)
            screen.blit(Cache.getImage(super().main_menu), (0, 12.071))

        # Info
        money = Cache.createTextDefaultFont(self.formattingValue(Player.money), self.default_font, 21, (0, 0, 0))
        bet = Cache.createTextDefaultFont(self.formattingValue(Player.bet), self.default_font, 21, (0, 0, 0))

        # Life
        for i in range(Player.life):
            screen.blit(Cache.getImage(super().heart), super().hearts[i])

        # Info
        screen.blit(money, (self.textPosition(Player.money, START, SPACE), 232))
        screen.blit(bet, (self.textPosition(Player.bet, START, SPACE), 361))
        
        # Avatar
        screen.blit(Cache.getImage(super().character), (535.884, 25.293))

    def openMenu(self, screen):
        menus = list(self.__active_menu.keys())

        for m in menus:
            if self.__active_menu.get(m):
                if m == "blackjack":
                    menu = f"{super().menu}/bj_menu.png"
                    x_freccia = (165.695, 267.464, 362.291)
                    y_freccia = 383.667
                    
                    if self.__animation:
                        # Positions for sub-menu animation
                        MENU_POS = 357.460
                        self.__y_sub_menu, arrow_position = self.animationOpenMenu(self.__y_sub_menu, MENU_POS, y_freccia, 5, bottom_to_up=True)

                        # Sub Menu
                        screen.blit(Cache.getImage(menu), (138.092, self.__y_sub_menu))
                        screen.blit(Cache.getImage(super().freccia_icon), (x_freccia[self.__index_x_freccia], arrow_position))

                        if self.__y_sub_menu == MENU_POS:
                            self.__animation = False
                    else:
                        # Sub Menu
                        self.__shift = y_freccia
                        screen.blit(Cache.getImage(menu), (138.092, self.__y_sub_menu))
                        screen.blit(Cache.getImage(super().freccia_icon), (x_freccia[self.__index_x_freccia], 383.667))

    def closeMenu(self, screen):
        menus = list(self.__active_menu.keys())

        for m in menus:
            if self.__active_menu.get(m):
                if m == "blackjack":
                    x_freccia = (165.695, 267.464, 362.291)

                    MENU_POS = 417

                    if self.__y_sub_menu < MENU_POS:
                        # Positions for sub-menu animation
                        self.__y_sub_menu, self.__shift = self.animationCloseMenu(self.__y_sub_menu, MENU_POS, self.__shift, 10, up_to_bottom=True)

                        # Sub Menu
                        screen.blit(Cache.getImage(self.__bj_menu), (138.092, self.__y_sub_menu))
                        screen.blit(Cache.getImage(super().freccia_icon), (x_freccia[self.__index_x_freccia], self.__shift))

    def choiceMenu(self, event, pressed_keys):
        menus = list(self.__active_menu.keys())

        for m in menus:
            if self.__active_menu.get(m):
                if m == "blackjack":
                    x_freccia = 2

                    if event.key is pygame.K_a:
                        if self.__index_x_freccia > 0:
                            self.__index_x_freccia -= 1
                        else:
                            self.__index_x_freccia = x_freccia
                    elif event.key is pygame.K_d:
                        if self.__index_x_freccia < x_freccia:
                            self.__index_x_freccia += 1
                        else:
                            self.__index_x_freccia = 0
                    elif event.key is pygame.K_RETURN:
                        if not self.__dealer_hand:
                            if self.__index_x_freccia == 0:
                                Cache.playEffect(self.__sound)
                                Player.hand["cards"].append( Deck.hit() )
                                Player.hand["value"] += Player.hand["cards"][ len(Player.hand["cards"]) - 1].value
                            elif self.__index_x_freccia == 1:
                                self.__dealer_hand = True
                                self.__close = True
                            else:
                                if Player.betting(Player.bet):
                                    Cache.playEffect(self.__sound)
                                    self.__dealer_hand = True
                                    self.__close = True

                                    while pygame.mixer.get_busy():
                                        pass

                                    Cache.playEffect(self.__sound)
                                    Player.hand["cards"].append( Deck.hit() )
                                    Player.hand["value"] += Player.hand["cards"][ len(Player.hand["cards"]) - 1].value
                                    Dealer.hand["cards"].append( Deck.hit() )
                                    Dealer.hand["value"] += Dealer.hand["cards"][ len(Dealer.hand["cards"]) - 1].value
                    elif event.key is pygame.K_BACKSPACE:
                        self.switchToScene(MainMenu())

                    return

    def baseHand(self):
        if(self.__first_call):
            Player.hand["cards"].append( Deck.hit() )
            Player.hand["value"] += Player.hand["cards"][0].value
            Dealer.hand["cards"].append( Deck.hit() )
            Dealer.hand["value"] += Dealer.hand["cards"][0].value
            Player.hand["cards"].append( Deck.hit() )
            Player.hand["value"] += Player.hand["cards"][1].value
            self.__first_call = False

    def dealing(self, screen):
        CARD_SPACE = 16
        
        # Cards position
        x_cards_dealer = (216.912, 228.912)
        x_cards_player = (216.912, 228.912, 244.912)

        y_two_rows_cards_dealer = (21.258, 49.258)
        y_one_row_cards_dealer = (35.258)

        y_three_rows_cards_player = (181.992, 209.992, 239.992)
        y_two_rows_cards_player = (196.992, 224.992)
        y_one_row_cards_player = (210.992)

        # Cards value position
        dealer_cards_value_position = (172.088, 70.242)
        player_cards_value_position = (172.088, 245.975)

        player_cards_value = Cache.createTextDefaultFont(str(Player.hand["value"]), self.default_font, 30, (255, 255, 255))

        if not self.__dealer_hand:
            if len(Player.hand["cards"]) > 20:
                for i in range(10):
                    x = x_cards_player[0] + (i * CARD_SPACE)
                    pos = (x, y_three_rows_cards_player[0])
                    # Card
                    screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                for i in range(10, 20):
                    x = x_cards_player[1] + ((i - 10) * CARD_SPACE)
                    pos = (x, y_three_rows_cards_player[1])
                    # Card
                    screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                for i in range(20, len(Player.hand["cards"])):
                    x = x_cards_player[2] + ((i - 20) * CARD_SPACE)
                    pos = (x, y_three_rows_cards_player[2])

                    if i == (len(Player.hand["cards"]) - 1):
                        if not pygame.mixer.get_busy():
                            # Card
                            screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                            screen.blit(player_cards_value, (player_cards_value_position[0], player_cards_value_position[1]))
                    else:
                        # Card
                        screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
            elif len(Player.hand["cards"]) > 10:
                for i in range(10):
                    x = x_cards_player[0] + (i * CARD_SPACE)
                    pos = (x, y_two_rows_cards_player[0])
                    # Card
                    screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                for i in range(10, len(Player.hand["cards"])):
                    x = x_cards_player[1] + ((i - 10) * CARD_SPACE)
                    pos = (x, y_two_rows_cards_player[1])

                    if i == (len(Player.hand["cards"]) - 1):
                        if not pygame.mixer.get_busy():
                            # Card
                            screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                            screen.blit(player_cards_value, (player_cards_value_position[0], player_cards_value_position[1]))
                    else:
                        # Card
                        screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
            else:
                for i in range(len(Player.hand["cards"])):
                    x = x_cards_player[0] + (i * CARD_SPACE)
                    pos = (x, y_one_row_cards_player)

                    if i == (len(Player.hand["cards"]) - 1):
                        if not pygame.mixer.get_busy():
                            # Card
                            screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                            screen.blit(player_cards_value, (player_cards_value_position[0], player_cards_value_position[1]))
                    else:
                        # Card
                        screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)

            for i in range(len(Dealer.hand["cards"])):
                dealer_cards_value = Cache.createTextDefaultFont(str(Dealer.hand["value"]), self.default_font, 30, (255, 255, 255))
                x = x_cards_dealer[0] + (i * CARD_SPACE)
                pos = (x, y_one_row_cards_dealer)
                # Card
                screen.blit(dealer_cards_value, (dealer_cards_value_position[0], dealer_cards_value_position[1]))
                screen.blit(Cache.getImage(Dealer.hand["cards"][i].img_path), pos)
        else:
            if Dealer.hand["value"] < 17:
                if not pygame.mixer.get_busy():
                    Cache.playEffect(self.__sound)
                    Dealer.hand["cards"].append( Deck.hit() )
                    Dealer.hand["value"] += Dealer.hand["cards"][ len(Dealer.hand["cards"]) - 1 ].value

            dealer_cards_value = Cache.createTextDefaultFont(str(Dealer.hand["value"]), self.default_font, 30, (255, 255, 255))

            if len(Dealer.hand["cards"]) > 10:
                for i in range(10):
                    x = x_cards_dealer[0] + (i * CARD_SPACE)
                    pos = (x, y_two_rows_cards_dealer[0])
                    # Card
                    screen.blit(Cache.getImage(Dealer.hand["cards"][i].img_path), pos)
                for i in range(10, len(Dealer.hand["cards"])):
                    x = x_cards_dealer[1] + ((i - 10) * CARD_SPACE)
                    pos = (x, y_two_rows_cards_dealer[1])

                    if i == (len(Dealer.hand["cards"]) - 1):
                        # Card
                        screen.blit(dealer_cards_value, (dealer_cards_value_position[0], dealer_cards_value_position[1]))
                        screen.blit(Cache.getImage(Dealer.hand["cards"][i].img_path), pos)
                    else:
                        screen.blit(Cache.getImage(Dealer.hand["cards"][i].img_path), pos)
            else:
                for i in range(len(Dealer.hand["cards"])):
                    x = x_cards_dealer[0] + (i * CARD_SPACE)
                    pos = (x, y_one_row_cards_dealer)

                    if i == (len(Dealer.hand["cards"]) - 1):
                        # Card
                        screen.blit(dealer_cards_value, (dealer_cards_value_position[0], dealer_cards_value_position[1]))
                        screen.blit(Cache.getImage(Dealer.hand["cards"][i].img_path), pos)
                    else:
                        screen.blit(Cache.getImage(Dealer.hand["cards"][i].img_path), pos)

            if len(Player.hand["cards"]) > 20:
                for i in range(10):
                    x = x_cards_player[0] + (i * CARD_SPACE)
                    pos = (x, y_three_rows_cards_player[0])
                    # Card
                    screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                for i in range(10, 20):
                    x = x_cards_player[1] + ((i - 10) * CARD_SPACE)
                    pos = (x, y_three_rows_cards_player[1])
                    # Card
                    screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                for i in range(20, len(Player.hand["cards"])):
                    x = x_cards_player[2] + ((i - 20) * CARD_SPACE)
                    pos = (x, y_three_rows_cards_player[2])
                    # Card
                    screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                    screen.blit(player_cards_value, (player_cards_value_position[0], player_cards_value_position[1]))
            elif len(Player.hand["cards"]) > 10:
                for i in range(10):
                    x = x_cards_player[0] + (i * CARD_SPACE)
                    pos = (x, y_two_rows_cards_player[0])
                    # Card
                    screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                for i in range(10, len(Player.hand["cards"])):
                    x = x_cards_player[1] + ((i - 10) * CARD_SPACE)
                    pos = (x, y_two_rows_cards_player[1])
                    # Card
                    screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                    screen.blit(player_cards_value, (player_cards_value_position[0], player_cards_value_position[1]))
            else:
                for i in range(len(Player.hand["cards"])):
                    x = x_cards_player[0] + (i * CARD_SPACE)
                    pos = (x, y_one_row_cards_player)
                    # Card
                    screen.blit(Cache.getImage(Player.hand["cards"][i].img_path), pos)
                    screen.blit(player_cards_value, (player_cards_value_position[0], player_cards_value_position[1]))
