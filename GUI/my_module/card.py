import os
import random

class Card:
    def __init__(self, value, suit, img_name='', img_path=''):
        self.__value = value
        self.__suit = suit
        self.__img_name = img_name
        self.__img_path = img_path.replace("/", os.sep)

    @property
    def value(self):
        return self.__value

    @property
    def suit(self):
        return self.__suit

    @property
    def img_name(self):
        return self.__img_name

    @property
    def img_path(self):
        return self.__img_path

class StandardDeck:
    def __init__(self, suits=["clubs", "diamonds", "hearts", "spades"], n_deck=1, n_card_per_deck=52, jolly=0, img_path="/img/cards/", img_ext=".png"):
        self.__suits = suits
        self.__n_deck = n_deck
        self.__n_card = n_card_per_deck
        self.__jolly = jolly
        self.__img_path = img_path
        self.__img_ext = img_ext
        self.__deck = self.__create()
        self.__deck_length = len(self.__deck)

    #Controllare creazioni oggetti Card -> si può rendere più efficente, guarda costruttore Music
    def __create(self):
        deck = []
        j_range = len(self.__suits)
        k_range = int(self.__n_card / len(self.__suits)) + 1

        for i in range(self.__n_deck):
            for j in range(j_range):
                for k in range(1, k_range):
                    file_name = f"0{k}{self.__img_ext}" if k < 10 else f"{k}{self.__img_ext}"
                    path = f"{self.__img_path}{self.__suits[j]}/{file_name}"
                    card = Card(k if k != 1 else 11, self.__suits[j], file_name, path) if k < 10 else Card(10, self.__suits[j], file_name, path)
                    deck.append(card)

        #test
        self.copy = deck

        return deck

    def shuffle(self):
        deck = self.__deck.copy()
        shuffled_deck = []
        length = len(deck)

        for i in range( length ):
            random_card = random.randrange(0, len(deck))
            shuffled_deck.append( deck[random_card] )
            deck.pop(random_card)

        self.__deck = shuffled_deck
        #test
        self.copy = self.__deck

    def hit(self):
        card = self.__deck.pop(0)
        self.__deck_length = len(self.__deck)
        return card

    @property
    def deck_length(self):
        return self.__deck_length

class BlackJackDeck(StandardDeck):
    def __init__(self, n_deck=4, img_path="/img/cards/"):
        StandardDeck.__init__(self, n_deck=n_deck, img_path=img_path)
