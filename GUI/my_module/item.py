class GenericItem:
    def __init__(self, item_type, name, cost, health):
        self.__type_of_item = item_type
        self.__name = name
        self.__cost = cost
        self.__health = health

    @property
    def type_of_item(self):
        return self.__type_of_item

    @property
    def name(self):
        return self.__name

    @property
    def cost(self):
        return self.__cost

    @property
    def health(self):
        return self.__health

    def use(self):
        pass

class Drug(GenericItem):
    def __init__(self, item_type="drug", name="cocaine", cost="45", health=2):
        super().__init__(item_type, name, cost, health)

    def use(self):
        return super().health