import os
import pygame

class Cache:
    __image_library = {}
    __cached_fonts = {}
    __cached_text = {}
    __effect_library = {}
    pygame.mixer.init(buffer=32)

    def getImage(self, path):
        image = self.__image_library.get(path)

        if image is None:
            canonicalized_path = path.replace("/", os.sep)
            image = pygame.image.load(canonicalized_path).convert_alpha()
            self.__image_library[path] = image

        return image

    def __makeFont(self, fonts, size):
        available = pygame.font.get_fonts()
        # get_fonts() returns a list of lowercase spaceless font names
        choices = map(lambda x: x.lower().replace(' ', ''), fonts)

        for choice in choices:
            if choice in available:
                return pygame.font.SysFont(choice, size)

        return pygame.font.Font(None, size)

    def __getFont(self, font_preferences, size):
        key = str(font_preferences) + '|' + str(size)
        font = self.__cached_fonts.get(key, None)

        if font is None:
            font = self.__makeFont(font_preferences, size)
            self.__cached_fonts[key] = font

        return font

    def createText(self, text, fonts, size, color):
        key = '|'.join(map(str, (fonts, size, color, text)))
        image = self.__cached_text.get(key, None)

        if image is None:
            font = self.__getFont(fonts, size)
            image = font.render(text, True, color)
            self.__cached_text[key] = image

        return image

    def createTextDefaultFont(self, text, myFont, size, color):
        canonicalized_path = myFont.replace("/", os.sep)
        font = os.path.basename(myFont).split('.')[0]

        key = '|'.join(map(str, (font, size, color, text)))
        image = self.__cached_text.get(key, None)

        if image is None:
            font = pygame.font.Font(canonicalized_path, size)
            image = font.render(text, True, color)
            self.__cached_text[key] = image

        return image

    def playEffect(self, path):
        sound = self.__effect_library.get(path)

        if sound is None:
            canonicalized_path = path.replace('/', os.sep)
            sound = pygame.mixer.Sound(canonicalized_path)
            self.__effect_library[path] = sound

        sound.play()

    def __load(self, path):
        folder = os.scandir(path)
        obj = {}
        arr = []

        for f in folder:
            if f.is_dir():
                obj[f.path] = self.__load(f.path)
            else:
                arr.append(f.name)

        if obj and arr:
            arr.append(obj)
            return arr
        elif obj:
            return obj
        elif arr:
            return arr
        else:
            return
