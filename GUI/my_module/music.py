import os
import random
import pygame

class Music():
    def __init__(self, path):
        self.__song_library = []

        for song in os.listdir(path):
            if song.endswith(".mp3"):
                song_path = os.path.join(path, song).replace('/', os.sep)
                self.__song_library.append(song_path)


    def playMusic(self):
        pygame.mixer.music.load(random.choice(self.__song_library))
        pygame.mixer.music.play()

    def stopMusic(self):
        pygame.mixer.music.stop()
