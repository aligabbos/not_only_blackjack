class Player():
    def __init__(self):
        self.__life = 5
        self.__money = 100
        self.__bet = 0
        self.__debts = 0
        self.__items = {}
        self.__hand = {
            "cards": [],
            "value": 0
        }

    @property
    def life(self):
        return self.__life

    @property
    def money(self):
        return self.__money

    @property
    def bet(self):
        return self.__bet

    @property
    def debts(self):
        return self.__debts

    @property
    def hand(self):
        return self.__hand

    @hand.setter
    def hand(self, val):
        self.__hand = val

    def clearHand(self):
        self.__hand["cards"].clear()
        self.__hand["blackjack"] = False
        self.__hand["value"] = 0

    def hasEnoughMoney(self, val):
        return True if self.__money >= val else False

    def hasBlackJack(self):
        if len(self.__hand["cards"]) == 2 and self.__hand["value"] == 21:
            return True
        else:
            return False

    def betting(self, bet):
        if self.__money >= bet:
            self.__bet += bet
            self.__money -= bet
            return True

        return False

    def clearBet(self):
        self.__money += self.__bet
        self.__bet = 0

    def winner(self):
        if self.hasBlackJack():
            self.__money += ( (self.__bet * 2) + self.__bet / 2 )
        else:
            self.__money += (self.__bet * 2)

        self.__bet = 0

    def loser(self):
        self.__bet = 0

    def payDebts(self):
        if self.__money >= self.__debts:
            self.__money -= self.__debts
            return True

        return False

    def buyItem(self, val, item):
        if self.hasEnoughMoney(val):
            item_type = self.__items.get(item.type_of_item)

            if item_type:
                self.__items[item.type_of_item].append(item_type)
            else:
                self.__items[item.type_of_item] = [item_type]
            
            return True
        else:
            return False

class Dealer():
    def __init__(self):
        self.__hand = {
            "cards": [],
            "blackjack": False,
            "value": 0
        }

    @property
    def hand(self):
        return self.__hand

    @hand.setter
    def hand(self, val):
        self.__hand = val

    def clearHand(self):
        self.__hand["cards"].clear()
        self.__hand["blackjack"] = False
        self.__hand["value"] = 0
